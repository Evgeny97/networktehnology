import os
import socket
import struct
import math
import itertools
import logging.config


def encryptor(data, key):
    """
        Шифровщик данных xor-ом
    """
    encrypted = []
    key = itertools.cycle(key)
    for d, k in zip(data, key):
        encrypted.append(d ^ k)
    return bytes(encrypted)


def checksum(msg):
    """
        Подсчет контрольной суммы пакета.
    """
    sum = 0
    msg_lenght = len(msg) & ((~0) << 1)
    count = 0
    while count < msg_lenght:
        thisVal = msg[count + 1] * 256 + msg[count]
        sum = sum + thisVal
        sum = sum & 0xffffffff
        count = count + 2

    if msg_lenght < len(msg):
        sum = sum + msg[len(msg) - 1]
        sum = sum & 0xffffffff

    sum = (sum >> 16) + (sum & 0xffff)
    sum = sum + (sum >> 16)
    answer = ~sum
    answer = answer & 0xffff
    # Swap bytes.
    answer = answer >> 8 | (answer << 8 & 0xff00)
    return answer


def send_file(file_name, dest_addr, data_size, do_encrypt, xor_key=b'0'):
    """
        Метод отправляющий фаил с именем file_name на адресс dest_addr.
        data_size - максимальный размер данных в одном пакете.
        data_size - нужно ли шифровать данные.
        xor_key - ключ которым шифруются данные
    """
    try:
        my_file = open(file_name, "rb")
    except:
        logger.error("Can't open file: " + file_name)
        print("Can't open file: " + file_name)
        exit(1)

    s1 = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
    s1.settimeout(5)

    # рабочий но кривой способ получить свой ip
    # s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # s.connect(("gmail.com", 80))
    # host =s.getsockname()[0]
    # s.close()

    # socket.gethostbyname(socket.gethostname())
    # my_ip = "192.168.1.34"
    # print(socket.gethostbyname_ex(socket.gethostname()))

    my_ID = os.getpid() & 0xFFFF
    dest_addr = socket.gethostbyname(dest_addr)
    # print(type(dest_addr))
    my_checksum = 0
    # хейдер без с нулевой контрольной суммой для следующего расчета этой суммы
    empy_data_header = struct.pack("bbHHh", 8, 0, my_checksum, my_ID, 1)
    file_size = os.path.getsize(file_name)
    packets_count = math.ceil(file_size / data_size)
    # запрос к серверу на отправление файла в packets_count пакетах с или без кодирования
    data = struct.pack("12si?", b'startNEWfile', packets_count, do_encrypt)
    my_checksum = checksum(empy_data_header + data + file_name.encode("utf-8"))
    # по порядку с тип, код , контрольная сумма, (идентификатор и номер последовательности у меня не используются никак).
    header = struct.pack("bbHHh", 8, 0, socket.htons(my_checksum), my_ID, 1)
    packet = header + data + file_name.encode("utf-8")
    s1.sendto(packet, (dest_addr, 1))
    success_send = False
    try:
        # попытки поймать ответ от сервера в 100 следующий icmp пакетах
        receive_count = 100;
        while 1:
            if receive_count == 0:
                logger.error("Can't connect server")
                print("Can't connect server")
                exit(1)
            receive_count -= 1
            receive_data = s1.recvfrom(65535)
            ip_header = receive_data[0][0:20]
            # распаковка ip заголовка пакета
            ip_struct = struct.unpack('!BBHHHBBH4s4s', ip_header)
            # проверка от сервера ли пакет
            if socket.inet_ntoa(ip_struct[8]) == dest_addr:
                if receive_data[0][28:47] == b'readytoacceptfile':
                    success_send = True
                    logger.info("Connection active")
                    print("connection active")
                    break
    except socket.error as e:
        # прошло 5 сек без принимания каких-либо icmp пакетов
        logger.error("Can't connect to: " + dest_addr)
        print("Can't connect to: " + dest_addr)
        exit(1)
    if not success_send:
        logger.error("Can't connect to: " + dest_addr)
        print("Can't connect to: " + dest_addr)
        exit(1)
    data = my_file.read(data_size)
    if do_encrypt:
        data = encryptor(data, xor_key)
    packet_number = 0
    # основной цикл отправки
    while data != b'':
        packet_number += 1
        data = struct.pack("I", packet_number) + data
        my_checksum = checksum(empy_data_header + data)
        header = struct.pack("bbHHh", 8, 0, socket.htons(my_checksum), my_ID, 1)
        packet = header + data
        send_attempts = 0
        success_send = False
        # 5 попыток отправить пакет, каждая следующая проводиться после неполучения ответа от сервера
        while send_attempts < 5:
            send_attempts += 1
            s1.sendto(packet, (dest_addr, 0))
            # эта процедура уже была описана выше
            try:
                receive_count = 100;
                while receive_count > 0:
                    receive_count -= 1
                    receive_data = s1.recvfrom(65535)
                    ip_header = receive_data[0][0:20]
                    ip_struct = struct.unpack('!BBHHHBBH4s4s', ip_header)
                    if socket.inet_ntoa(ip_struct[8]) == dest_addr:
                        if receive_data[0][28:35] == b'success':
                            unpacked_struct = struct.unpack("7sI", receive_data[0][28:])
                            if unpacked_struct[1] == packet_number:
                                print(file_name, " progress: ", int(packet_number / packets_count * 100), "%")
                                logger.debug(
                                    file_name + " progress: " + str(int(packet_number / packets_count * 100)) + "%")
                                success_send = True
                                break
                if not success_send:
                    print("server stop answer next attempt number:", send_attempts)
                    logger.error("server stop answer next attempt number:" + str(send_attempts))
                else:
                    break
            except socket.error as e:
                print("Timeout. Server stop answer. Trying to send the package again (", send_attempts, "/ 5 )")
                logger.error("Timeout. Server stop answer. Trying to send the package again (" + str(send_attempts) + "/ 5 )")
        if not success_send:
            print("Server stop answer can't send file: " + file_name)
            logger.error("Server stop answer can't send file: " + file_name)
            exit(1)
        # раньше было что перееполнялся буфер сокета при отправке кучи пакетов подряд, но после добавления ожидания ответа
        # прошло и sleep не нужен стал
        # time.sleep(1)
        data = my_file.read(data_size)
        if do_encrypt:
            data = encryptor(data, xor_key)
    print("Complete sending")
    logger.info("Complete sending")
    my_file.close()
    return

# настройки логера
dictLogConfig = {
    "version": 1,
    "handlers": {
        "fileHandler": {
            "class": "logging.FileHandler",
            "formatter": "myFormatter",
            "filename": "log.log"
        }
    },
    "loggers": {
        "client": {
            "handlers": ["fileHandler"],
            "level": "INFO",
        }
    },
    "formatters": {
        "myFormatter": {
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        }
    }
}

logging.config.dictConfig(dictLogConfig)
logger = logging.getLogger("client")

# ключ может быть вообще любой набор бит, но должен совпадать с сервером
xor_key = b'328gm45idsnq15AEWRT2QQ11fa'
dest_addr = "192.168.1.34"
file_name = "in.iso"
data_size = 60000
do_encrypt = True

send_file(file_name, dest_addr, data_size, do_encrypt, xor_key)
