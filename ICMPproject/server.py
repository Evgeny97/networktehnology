import socket
import struct
import itertools
import logging.config
import os
import datetime


class FileWriter:
    """
         Класс отвечающий за запись в файл.
    """

    def __init__(self, sender, _file_name, packets_count):
        folder_name = str("from " + sender + ", time " + datetime.datetime.now().strftime("%H-%M-%S %d-%m-%Y"))
        print(type(folder_name))
        # папка создаеться для определения когда и от кого получен файл
        os.mkdir(folder_name)
        self.file = open(folder_name + "/" + _file_name.decode(), "wb")
        self.file_name = _file_name.decode()
        self.packets_count = packets_count
        self.packets_writed = 0

    def getPacketNumber(self):
        """
            getter для количества записанных блоков.
        """
        return self.packets_writed

    def write(self, pac_number, data):
        """
            Сама запись в фаил.
        """
        if not self.file.closed:
            if pac_number == self.packets_writed + 1:
                self.file.write(data)
                self.packets_writed += 1
                print(self.file_name, " progress: ",
                      int(self.packets_writed / self.packets_count * 100), "%")
                logger.debug(self.file_name + " progress: " +
                            str(int(self.packets_writed / self.packets_count * 100)) + "%")
                if self.packets_writed == self.packets_count:
                    self.file.close()
                    print("file: ", self.file_name, " complete")
                    logger.info("file: \"" + self.file_name + "\" complete")
                    return True
                else:
                    return False


def encryptor(data, key):
    """
        расшифровщик
    """
    encrypted = []
    key = itertools.cycle(key)
    for d, k in zip(data, key):
        encrypted.append(d ^ k)
    return bytes(encrypted)


def checksum(msg):
    """
        Подсчет контрольной суммы пакета.
    """
    sum = 0
    msg_lenght = len(msg) & ((~0) << 1)
    count = 0
    while count < msg_lenght:
        thisVal = msg[count + 1] * 256 + msg[count]
        sum = sum + thisVal
        sum = sum & 0xffffffff
        count = count + 2

    if msg_lenght < len(msg):
        sum = sum + msg[len(msg) - 1]
        sum = sum & 0xffffffff

    sum = (sum >> 16) + (sum & 0xffff)
    sum = sum + (sum >> 16)
    answer = ~sum
    answer = answer & 0xffff
    # Swap bytes.
    answer = answer >> 8 | (answer << 8 & 0xff00)
    return answer


def listen(host, xor_key):
    """
        Метод приема файлов.
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
    # прослушиваем свой адресс
    s.bind((host, 0))
    print("Server started at ip: " + str(host))
    logger.info("Server started at ip: " + str(host))
    # для Windows
    # s.ioctl(socket.SIO_RCVALL, socket.RCVALL_ON)
    # словарь (адресс отправителя) - (FileWriter)
    d = {}
    do_encrypt = False
    while 1:
        data = s.recvfrom(65535)
        ip_header = data[0][0:20]
        icmp_header = data[0][20:28]
        icmp_empty_header = icmp_header[0:2] + struct.pack('!H', 0) + icmp_header[4:8]
        ip_struct = struct.unpack('!BBHHHBBH4s4s', ip_header)
        icmp_struct = struct.unpack('!bbHHh', icmp_header)
        if socket.inet_ntoa(ip_struct[9]) == host:
            # при отправлении контрольная сумма проходит через socket.htons а прииходит почему то нормальная(не требующая socket.ntohs)
            # print(socket.ntohs(icmp_struct[2]), "  ",icmp_struct[2]," " , checksum(icmp_empty_header + data[0][28:]))
            if data[0][28:40] == b'startNEWfile':
                # получения запроса на запись нового файла, количества пакетов, и будет ли кодироваться
                unpacked = struct.unpack("12si?", data[0][28:45])
                d[data[1][0]] = FileWriter(data[1][0], data[0][45:], unpacked[1])
                do_encrypt = unpacked[2]
                empy_data_header = struct.pack("bbHHh", 0, 0, 0, 99, 1)
                my_checksum = checksum(empy_data_header + b'readytoacceptfile')
                header = struct.pack("bbHHh", 0, 0, socket.htons(my_checksum), 99, 1)
                packet = header + b'readytoacceptfile'
                s.sendto(packet, (data[1][0], 1))
                logger.info(
                    "Start receiving new file: \"" + data[0][45:].decode("utf-8") + "\" from " + str(data[1][0]))
            else:
                if d.get(data[1][0]) is not None:
                    if icmp_struct[2] == checksum(icmp_empty_header + data[0][28:]):
                        if do_encrypt:
                            d[data[1][0]].write(struct.unpack("I", data[0][28:32])[0], encryptor(data[0][32:], xor_key))
                        else:
                            d[data[1][0]].write(struct.unpack("I", data[0][28:32])[0], data[0][32:])
                        empy_data_header = struct.pack("bbHHh", 0, 0, 0, 99, 1)
                        # отчет о том, что успешно получен d[data[1][0]].getPacketNumber() пакет
                        local_data = struct.pack("7sI", b'success', d[data[1][0]].getPacketNumber())
                        my_checksum = checksum(empy_data_header + local_data)
                        header = struct.pack("bbHHh", 0, 0, socket.htons(my_checksum), 99, 1)
                        packet = header + local_data
                        s.sendto(packet, (data[1][0], 1))

# настройки логера
dictLogConfig = {
    "version": 1,
    "handlers": {
        "fileHandler": {
            "class": "logging.FileHandler",
            "formatter": "myFormatter",
            "filename": "log.log"
        }
    },
    "loggers": {
        "server": {
            "handlers": ["fileHandler"],
            "level": "INFO",
        }
    },
    "formatters": {
        "myFormatter": {
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        }
    }
}

logging.config.dictConfig(dictLogConfig)
logger = logging.getLogger("server")

xor_key = b'328gm45idsnq15AEWRT2QQ11fa'
my_ip = "192.168.1.33"

listen(my_ip, xor_key)
